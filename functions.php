<?php declare (strict_types = 1);

const DATABASE = 'baltic_talents';
const HOST = 'localhost';
const DB_USER = 'root';
const DB_PASSWORD = 'lunatikas';

const NPD = 149;
const INCOME_TAX_PERCENT = 0.15;
const HEALTH_INSURANCE_PERCENT = 0.06;
const SOCIAL_INSURANCE_PERCENT = 0.06;
const SODRA_PERCENT = 0.3098;
const WARANTY_FUND_PERCENT = 0.002;

function getConnection(): PDO
{
    return new PDO('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
}

function getAllEmployees(PDO $pdo): array
{
    $stmt = $pdo->prepare('SELECT id, name, surname, education, salary, phone FROM darbuotojai');
    $stmt->execute();

    return $stmt->fetchAll();
}

function getEmployeesByPosition(PDO $pdo, int $positionId): array
{
    $stmt = $pdo->prepare('SELECT id, name, surname, education, salary, phone FROM darbuotojai WHERE pareigos_id = :position_id ');
    $stmt->execute(['position_id' => $positionId]);

    return $stmt->fetchAll();
}

function getPosition(PDO $pdo, int $positionId): array
{
    $stmt = $pdo->prepare('SELECT id, name, base_salary FROM pareigos WHERE id =:id');
    $stmt->execute(['id' => $positionId]);

    return $stmt->fetch();
}

function getEmployee(PDO $pdo, int $employeeId): array
{
    $stmt = $pdo->prepare('SELECT id, name, surname, education, salary, phone FROM darbuotojai WHERE id =:id');
    $stmt->execute(['id' => $employeeId]);
    return $stmt->fetch();
}

function getAllPositions(PDO $pdo): array
{
    $stmt = $pdo->prepare('SELECT id, name, base_salary FROM pareigos');
    $stmt->execute();

    return $stmt->fetchAll();
}

function getSalaryData(float $salary): array
{
    $incomeTax = ($salary - NPD) * INCOME_TAX_PERCENT;
    $healthSecurityTax = $salary * HEALTH_INSURANCE_PERCENT;
    $socialSecurityTax = $salary * SOCIAL_INSURANCE_PERCENT;
    $netSalary = $salary - $incomeTax - $healthSecurityTax - $socialSecurityTax;

    return [
        'income_tax' => $incomeTax,
        'health_security_tax' => $healthSecurityTax,
        'social_security_tax' => $socialSecurityTax,
        'net_salary' => $netSalary,
    ];
}

function getExpenses(float $salary): array
{
    return [
        'sodra' => $salary * SODRA_PERCENT,
        'warranty_fund' => $salary * WARANTY_FUND_PERCENT,
        'salary' => $salary,
        'total' => $salary * SODRA_PERCENT + $salary * WARANTY_FUND_PERCENT + $salary,
    ];
}
